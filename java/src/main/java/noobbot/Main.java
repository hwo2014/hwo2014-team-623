package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Main {
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as "
				+ botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
				socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));
	}

	final Gson gson = new Gson();
	private PrintWriter writer;
	private static String botName = null;
	
	public Main(final BufferedReader reader, final PrintWriter writer,
			final Join join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		GameInit gameInit = null;
		boolean turboAvailable = false;
		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line,
					MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {
				Position[] positions = gson.fromJson(
						msgFromServer.dataAsJson(), Position[].class);

				for (Position position : positions) {
					if (position.id.name.equals(botName)) {
						System.out.println(position.toString());
						Piece piece = gameInit.race.track.pieces[position.piecePosition.pieceIndex];
						double thro = 0.35;
						if (piece.length > 0) {
							double perc = 1 - ((position.piecePosition.inPieceDistance * 100) / piece.length) / 100;
							if (perc > 0.3) {
								send(new Throttle(1.0));
								break;
							} else {
								thro = 0.3 + 0.5 * (((perc * 100) / 0.3) / 100);
							}
						}
						if (turboAvailable) {
							thro /= 3;
						}
						send(new Throttle(thro));
						break;
					}
				}

			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("Joined");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("Race init");

				gameInit = gson.fromJson(msgFromServer.dataAsJson(),
						GameInit.class);
				// for (Piece piece : gameInit.race.track.pieces) {
				// System.out.println(piece);
				// }
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("Race start");
			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("Car crashed");
				System.exit(-1);
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				System.out.println("Turbo Available!");
				turboAvailable = true;
				send(new Turbo());
			} else {
				System.out.println(">" + msgFromServer.msgType + " "
						+ msgFromServer.data);
				send(new Ping());
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}

	public String dataAsJson() {
		return new Gson().toJson(data);
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Turbo extends SendMsg {
	@Override
	protected String msgType() {
		return "turbo";
	}

	@Override
	protected Object msgData() {
		return "Uskorenie!";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}

class Position {
	Id id;
	double angle;
	PiecePosition piecePosition;

	Position(Id id, double angle, PiecePosition piecePosition) {
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}

	@Override
	public String toString() {
		return "[angle=" + angle + ", position=" + piecePosition + "]";
	}
}

class Id {
	String name;
	String color;

	Id(final String name, final String color) {
		this.name = name;
		this.color = color;
	}
}

class PiecePosition {
	int pieceIndex;
	double inPieceDistance;
	Lane lane;
	int lap;

	@Override
	public String toString() {
		return "[pieceIndex=" + pieceIndex + ", inPieceDistance="
				+ inPieceDistance + ", lane=" + lane + ", lap=" + lap + "]";
	}

	public PiecePosition(int pieceIndex, double inPieceDistance, Lane lane,
			int lap) {
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
}

class Lane {
	int startLaneIndex;
	int endLaneIndex;

	Lane(int startLaneIndex, int endLaneIndex) {
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}

	@Override
	public String toString() {
		return startLaneIndex + "_" + endLaneIndex;
	}
}

class GameInit {
	Race race;

	GameInit(Race race) {
		this.race = race;
	}
}

class Race {
	Track track;

	Race(Track track) {
		this.track = track;
	}
}

class Track {
	Piece[] pieces;

	Track(Piece[] pieces) {
		this.pieces = pieces;
	}
}

class Piece {
	double length;
	@SerializedName("switch")
	boolean canSwitch;
	int radius;
	double angle;

	public Piece(double length, boolean canSwitch, int radius, double angle) {
		this.length = length;
		this.canSwitch = canSwitch;
		this.radius = radius;
		this.angle = angle;
	}

	@Override
	public String toString() {
		return "Piece [length=" + length + ", canSwitch=" + canSwitch
				+ ", radius=" + radius + ", angle=" + angle + "]";
	}
}

class TurboAvailable {
	double turboDurationMilliseconds;
	int turboDurationTicks;
	double turboFactor;

	TurboAvailable(double turboDurationMilliseconds, int turboDurationTicks,
			double turboFactor) {
		this.turboDurationMilliseconds = turboDurationMilliseconds;
		this.turboDurationTicks = turboDurationTicks;
		this.turboFactor = turboFactor;
	}
}